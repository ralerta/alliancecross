package com.alliance.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.alliance.entity.Donor;
import com.alliance.repository.DonorRepository;

@Service
public class DonorService {

	@Autowired
	public DonorRepository donorRepository; 
	
	public List<Donor> getAllDonors(Donor donor)
	{
		List<Donor> donorList = donorRepository.findAll();
		
		return donorList;
	}
	
	public Donor createDonor(@Valid @RequestBody Donor donor) {
	    return donorRepository.save(donor);
	}
	
	public Donor getDonorsById(Long donorID) {
	    return donorRepository.findOne(donorID);
	}
	
	public Donor updateDonor( Long donorID, Donor donorDetails) {

			return donorRepository.findOne(donorID);
	}
	
	public Donor updateDonor(@Valid @RequestBody Donor donor) {
	    return donorRepository.save(donor);
	}
	
	public ResponseEntity<?> deleteDonor(@Valid @RequestBody Donor donor)
	{
		donorRepository.delete(donor);
		return ResponseEntity.ok().build();
	}
	
	public List <Donor> getAllCompatibleA(){
		return donorRepository.getCompatibleA();		
	}

	public List <Donor> getAllCompatibleO(){
		return donorRepository.getCompatibleO();	
	}

	public List <Donor> getAllCompatibleB(){
		return donorRepository.getCompatibleB();			
	}

	public List <Donor> getAllCompatibleAminus(){
		return donorRepository.getCompatibleAminus();
	}

	public List <Donor> getAllCompatibleBminus(){
		return donorRepository.getCompatibleBminus();	
	}

	public List <Donor> getAllCompatibleABminus(){
		return donorRepository.getCompatibleABminus();	
	}
}
