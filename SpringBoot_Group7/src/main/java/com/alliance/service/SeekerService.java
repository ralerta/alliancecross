package com.alliance.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.alliance.entity.Seeker;
import com.alliance.repository.SeekerRepository;

@Service
public class SeekerService {

	@Autowired
	public SeekerRepository seekerRepository; 
	
	public List<Seeker> getAllSeekers(Seeker seeker)
	{
		List<Seeker> seekerList = seekerRepository.findAll();
		
		return seekerList;
	}
	
	public Seeker createSeeker(@Valid @RequestBody Seeker seeker) {
	    return seekerRepository.save(seeker);
	}
	
	public Seeker getSeekersById(Long seekerID) {
	    return seekerRepository.findOne(seekerID);
	}
	
	public Seeker updateSeeker( Long seekerID, Seeker seekerDetails) {

			return seekerRepository.findOne(seekerID);
	}
	
	public Seeker updateSeeker(@Valid @RequestBody Seeker seeker) {
	    return seekerRepository.save(seeker);
	}
	
	public ResponseEntity<?> deleteSeeker(@Valid @RequestBody Seeker seeker)
	{
		seekerRepository.delete(seeker);
		return ResponseEntity.ok().build();
	}
}
