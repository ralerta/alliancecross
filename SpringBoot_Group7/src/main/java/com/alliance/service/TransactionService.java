package com.alliance.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.alliance.entity.Transaction;
import com.alliance.repository.TransactionRepository;

@Service
public class TransactionService {

	@Autowired
	public TransactionRepository transactionRepository; 
	
	public List<Transaction> getAllTransactions(Transaction transaction)
	{
		List<Transaction> transactionList = transactionRepository.findAll();
		
		return transactionList;
	}
	
	public Transaction createTransaction(@Valid @RequestBody Transaction transaction) {
	    return transactionRepository.save(transaction);
	}
	
	public Transaction getTransactionsById(Long transactionID) {
	    return transactionRepository.findOne(transactionID);
	}
	
	public Transaction updateTransaction( Long transactionID, Transaction transactionDetails) {

			return transactionRepository.findOne(transactionID);
	}
	
	public Transaction updateTransaction(@Valid @RequestBody Transaction transaction) {
	    return transactionRepository.save(transaction);
	}
	
	public ResponseEntity<?> deleteTransaction(@Valid @RequestBody Transaction transaction)
	{
		transactionRepository.delete(transaction);
		return ResponseEntity.ok().build();
	}
}
