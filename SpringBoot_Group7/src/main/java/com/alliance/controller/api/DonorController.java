package com.alliance.controller.api;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import com.alliance.entity.Donor;
import com.alliance.service.DonorService;


import org.springframework.web.bind.annotation.*;

@RestController(value="DonorController")
@RequestMapping("/api")
public class DonorController {
	
	@Autowired
    private DonorService donorService;
     
	@GetMapping("/donors")
	public List<Donor> getAllDonors(Donor donor) 
	{
	    return donorService.getAllDonors(donor);
	}
	
	@PostMapping("/donors")
	public Donor createDonor(@Valid @RequestBody Donor donor) {
	    return donorService.createDonor(donor);
	}
	
	@GetMapping("/donors/{id}")
	public Donor getDonorsById(@PathVariable(value = "id") Long donorID) {
	    return donorService.getDonorsById(donorID);
	}
	
	@RequestMapping("/donors/update/{id}")
	public Donor updateDonor(@PathVariable(value = "id") Long donorID,
	                                        @Valid @RequestBody Donor donorDetails) {
		
		Donor donor = donorService.getDonorsById(donorID);
	        
		donor.setFirstName(donorDetails.getFirstName());
		donor.setLastName(donorDetails.getLastName());
		donor.setAddress(donorDetails.getAddress());
		donor.setContactNumber(donorDetails.getContactNumber());
		donor.setEmailAddress(donorDetails.getEmailAddress());
		donor.setPassword(donorDetails.getPassword());
		

	    Donor updatedDonor = donorService.updateDonor(donor);
	    return updatedDonor;
	}
	
	@RequestMapping("/donors/delete/{id}")
	public ResponseEntity<?> deleteDonor(@PathVariable (value = "id") Long donorID) {
		
		Donor donor = donorService.getDonorsById(donorID);
	    
		return donorService.deleteDonor(donor);
	    
	}
	
	@RequestMapping(value = "/getAllCompatibleA",method = RequestMethod.GET)
	public List<Donor> getAllCompatibleA() {
	List<Donor> getDonorList = new ArrayList<>();
	getDonorList = donorService.getAllCompatibleA();	
	return getDonorList;
	}
	
	@RequestMapping(value = "/getAllCompatibleO",method = RequestMethod.GET)
	public List<Donor> getAllCompatibleO() {
	List<Donor> getDonorList = new ArrayList<>();
	getDonorList = donorService.getAllCompatibleO();	
	return getDonorList;
	}
	
	@RequestMapping(value = "/getAllCompatibleB",method = RequestMethod.GET)
	public List<Donor> getAllCompatibleB() {
	List<Donor> getDonorList = new ArrayList<>();
	getDonorList = donorService.getAllCompatibleB();	
	return getDonorList;
	}
	
	@RequestMapping(value = "/getAllCompatibleAminus",method = RequestMethod.GET)
	public List<Donor> getAllCompatibleAminus() {
	List<Donor> getDonorList = new ArrayList<>();
	getDonorList = donorService.getAllCompatibleAminus();	
	return getDonorList;
	}
	
	@RequestMapping(value = "/getAllCompatibleBminus",method = RequestMethod.GET)
	public List<Donor> getAllCompatibleBminus() {
	List<Donor> getDonorList = new ArrayList<>();
	getDonorList = donorService.getAllCompatibleBminus();	
	return getDonorList;
	}
	
	@RequestMapping(value = "/getAllCompatibleABminus",method = RequestMethod.GET)
	public List<Donor> getAllCompatibleABminus() {
	List<Donor> getDonorList = new ArrayList<>();
	getDonorList = donorService.getAllCompatibleABminus();	
	return getDonorList;
	}
 
}
