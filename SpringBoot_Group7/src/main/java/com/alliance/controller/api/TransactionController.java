package com.alliance.controller.api;

import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import com.alliance.entity.Transaction;
import com.alliance.service.TransactionService;


import org.springframework.web.bind.annotation.*;

@RestController(value="TransactionController")
@RequestMapping("/api")
public class TransactionController {
	
	@Autowired
    private TransactionService transactionService;
     
	@GetMapping("/transactions")
	public List<Transaction> getAllTransactions(Transaction transaction) 
	{
	    return transactionService.getAllTransactions(transaction);
	}
	
	@PostMapping("/transactions")
	public Transaction createTransaction(@Valid @RequestBody Transaction transaction) {
	    return transactionService.createTransaction(transaction);
	}
	
	@GetMapping("/transactions/{id}")
	public Transaction getTransactionsById(@PathVariable(value = "id") Long transactionID) {
	    return transactionService.getTransactionsById(transactionID);
	}
	
	@RequestMapping("/transactions/delete/{id}")
	public ResponseEntity<?> deleteTransaction(@PathVariable (value = "id") Long transactionID) {
		
		Transaction transaction = transactionService.getTransactionsById(transactionID);
	    
		return transactionService.deleteTransaction(transaction);
	    
	}
 
}
