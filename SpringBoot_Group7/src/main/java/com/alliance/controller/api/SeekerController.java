package com.alliance.controller.api;

import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import com.alliance.entity.Seeker;
import com.alliance.service.SeekerService;


import org.springframework.web.bind.annotation.*;

@RestController(value="SeekerController")
@RequestMapping("/api")
public class SeekerController {
	
	@Autowired
    private SeekerService seekerService;
     
	@GetMapping("/seekers")
	public List<Seeker> getAllSeekers(Seeker seeker) 
	{
	    return seekerService.getAllSeekers(seeker);
	}
	
	@PostMapping("/seekers")
	public Seeker createSeeker(@Valid @RequestBody Seeker seeker) {
	    return seekerService.createSeeker(seeker);
	}
	
	@GetMapping("/seekers/{id}")
	public Seeker getSeekersById(@PathVariable(value = "id") Long seekerID) {
	    return seekerService.getSeekersById(seekerID);
	}
	
	@RequestMapping("/seekers/update/{id}")
	public Seeker updateSeeker(@PathVariable(value = "id") Long seekerID,
	                                        @Valid @RequestBody Seeker seekerDetails) {
		
		Seeker seeker = seekerService.getSeekersById(seekerID);
	        
		seeker.setFirstName(seekerDetails.getFirstName());
		seeker.setLastName(seekerDetails.getLastName());
		seeker.setAddress(seekerDetails.getAddress());
		seeker.setContactNumber(seekerDetails.getContactNumber());
		seeker.setEmailAddress(seekerDetails.getEmailAddress());
		

	    Seeker updatedSeeker = seekerService.updateSeeker(seeker);
	    return updatedSeeker;
	}
	
	@RequestMapping("/seekers/delete/{id}")
	public ResponseEntity<?> deleteSeeker(@PathVariable (value = "id") Long seekerID) {
		
		Seeker seeker = seekerService.getSeekersById(seekerID);
	    
		return seekerService.deleteSeeker(seeker);
	    
	}
 
}
