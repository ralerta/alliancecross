package com.alliance.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import com.alliance.entity.Donor;

@Repository
public interface DonorRepository extends JpaRepository<Donor, Long> {
	
	@Query(value = "select j from Donor j WHERE j.BloodType LIKE 'A+' OR j.BloodType LIKE 'A-' OR j.BloodType LIKE 'O+'  OR j.BloodType LIKE 'O-' ")
	List<Donor> getCompatibleA();
	
	@Query(value = "select j from Donor j WHERE j.BloodType LIKE 'O+' OR j.BloodType LIKE 'O-' ")
	List<Donor> getCompatibleO();
	
	@Query(value = "select j from Donor j WHERE j.BloodType LIKE 'B+' OR j.BloodType LIKE 'B-'  OR j.BloodType LIKE 'O+' OR j.BloodType LIKE 'O-' ")
	List<Donor> getCompatibleB();
	
	@Query(value = "select j from Donor j WHERE j.BloodType LIKE 'A-' OR j.BloodType LIKE 'O-' ")
	List<Donor> getCompatibleAminus();
	
	@Query(value = "select j from Donor j WHERE j.BloodType LIKE 'B+' OR j.BloodType LIKE 'B-' OR j.BloodType LIKE 'O-' ")
	List<Donor> getCompatibleBminus();
	
	@Query(value = "select j from Donor j WHERE j.BloodType LIKE 'AB+' OR j.BloodType LIKE 'AB-' ")
	List<Donor> getCompatibleABminus();
}
