package com.alliance.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.alliance.entity.Seeker;

@Repository
public interface SeekerRepository extends JpaRepository<Seeker, Long> {}
