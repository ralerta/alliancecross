package com.alliance.entity;


import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@Entity
@Table(name = "Seeker")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, 
        allowGetters = true)
public class Seeker{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long ID;
	
	
	//bi-directional one-to-many association to Transaction
	@OneToMany(fetch = FetchType.LAZY, mappedBy="seeker")
	private List<Transaction> transactionLists;
	

	@NotBlank
	private String LastName;
	
	public List<Transaction> getTransactionLists() {
		return transactionLists;
	}

	public void setTransactionLists(List<Transaction> transactionLists) {
		this.transactionLists = transactionLists;
	}

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getBloodType() {
		return BloodType;
	}

	public void setBloodType(String bloodType) {
		BloodType = bloodType;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getContactNumber() {
		return ContactNumber;
	}

	public void setContactNumber(String contactNumber) {
		ContactNumber = contactNumber;
	}

	public String getEmailAddress() {
		return EmailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		EmailAddress = emailAddress;
	}

	public int getUserType() {
		return UserType;
	}

	public void setUserType(int userType) {
		UserType = userType;
	}

	@NotBlank
	private String FirstName;
	
	@NotBlank
	private String BloodType;
	
	@NotBlank
	private String Address;
	
	@NotBlank
	private String ContactNumber;
	
	@NotBlank
	private String EmailAddress;
	
	@NotNull
	private int UserType;
}
