package com.alliance.entity;

import java.util.List;


import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "Donor")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, allowGetters = true)
public class Donor {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long ID;
	

	//bi-directional one-to-many association to Transaction
	@OneToMany(fetch = FetchType.LAZY, mappedBy="donor")
	private List<Transaction> transactionLists;
		
	public List<Transaction> getTransactionLists() {
		return transactionLists;
	}

	public void setTransactionLists(List<Transaction> transactionLists) {
		this.transactionLists = transactionLists;
	}

	@NotBlank
	private String LastName;
	
	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public String getLastName() {
		return LastName;
	}

	

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getBloodType() {
		return BloodType;
	}

	public void setBloodType(String bloodType) {
		BloodType = bloodType;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getContactNumber() {
		return ContactNumber;
	}

	public void setContactNumber(String contactNumber) {
		ContactNumber = contactNumber;
	}

	public String getEmailAddress() {
		return EmailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		EmailAddress = emailAddress;
	}
	
	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		EmailAddress = password;
	}
	
	

	

	@NotBlank
	private String FirstName;
	
	@NotBlank
	private String BloodType;
	
	@NotBlank
	private String Address;
	
	@NotBlank
	private String ContactNumber;
	
	@NotBlank
	private String EmailAddress;
	
	@NotBlank
	private String Password;
	

	
}
