package com.alliance.entity;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "Transaction")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, 
        allowGetters = true)
public class Transaction {
	
	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}


	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getSchedule() {
		return schedule;
	}

	public void setSchedule(Date schedule) {
		this.schedule = schedule;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long ID;
	
	
	@OneToOne(fetch = FetchType.LAZY)
    @MapsId
    private Seeker seeker;
	
	public Seeker getSeeker() {
		return seeker;
	}

	public void setSeeker(Seeker seeker) {
		this.seeker = seeker;
	}

	public Donor getDonor() {
		return donor;
	}

	public void setDonor(Donor donor) {
		this.donor = donor;
	}

	@OneToOne(fetch = FetchType.LAZY)
    @MapsId
    private Donor donor;
	
	
	@NotNull
	private int status;
	
	@Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
	private Date schedule;

}
